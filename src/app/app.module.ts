import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'

import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DropdownlistComponent } from './dropdownlist/dropdownlist.component';

@NgModule({
  declarations: [
    AppComponent,
    DropdownlistComponent
  ],
  imports: [
    BrowserModule,FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
