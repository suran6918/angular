import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-dropdownlist',
  templateUrl: './dropdownlist.component.html',
  styleUrls: ['./dropdownlist.component.css']
})
export class DropdownlistComponent implements OnInit {
  selectedItem = 'https://via.placeholder.com';
  myWidth=100
  myHeight = 100
  combined = ''
  constructor() {     
  }

  ngOnInit() {
    this.combined=`${this.selectedItem}/${this.myWidth}/${this.myHeight}`
  }

  selectItem(event: any) {
    this.selectedItem = event.target.value;
  }

  submitUrl(event: any){
    this.combined=`${this.selectedItem}/${this.myWidth}/${this.myHeight}`
  }
  
  urlOptions = [
    { valueUrl: 'https://via.placeholder.com', optionText: 'placeholder' },
    { valueUrl: 'https://picsum.photos', optionText: 'general' },
    { valueUrl: 'https://placekitten.com', optionText: 'kitten' },
    { valueUrl: 'https://placebear.com', optionText: 'bear' },
    { valueUrl: 'https://placebeard.it', optionText: 'beard' },
    { valueUrl: 'http://www.fillmurray.com', optionText: 'Phil' },
    { valueUrl: 'https://www.placecage.com', optionText: 'Nicholas' },
    { valueUrl: 'https://stevensegallery.com', optionText: 'Steven' }
  ]

  testvalueUrl = 'https://placekitten.com/200/300'

 
}
